<?php

namespace Steam\CounterStrike\Application;

use Steam\CounterStrike\Domain\PlayerStatsRepository;

class PlayerStatisticService
{
    private $playerStatsRepository;
    private $persistencePlayerStatsRepository;

    public function __construct(
        PlayerStatsRepository $httpPlayerStatsRepository,
        PlayerStatsRepository $persistencePlayerStatsRepository)
    {
        $this->playerStatsRepository = $httpPlayerStatsRepository;
        $this->persistencePlayerStatsRepository = $persistencePlayerStatsRepository;
    }

    public function fetchNewPlayerData(FetchPlayerDataCommand $command)
    {
        $playerId = $command->playerId();

        $steamPlayerStats = $this->playerStatsRepository->findLastForPlayer($playerId);
        $persistedPlayerStats = $this->persistencePlayerStatsRepository->findLastForPlayer($playerId);

        if (is_null($steamPlayerStats) || $steamPlayerStats->equals($persistedPlayerStats)){
            return; // no new data found
        }

        $this->persistencePlayerStatsRepository->save($steamPlayerStats);

//        $this->eventBus->publish(new PlayerStatsFetched($steamPlayerStats));
    }
}
