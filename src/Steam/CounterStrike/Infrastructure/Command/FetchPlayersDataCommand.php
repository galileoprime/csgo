<?php

namespace Steam\CounterStrike\Infrastructure\Command;

use Exception;
use Steam\CounterStrike\Application\FetchPlayerDataCommand;
use Steam\CounterStrike\Application\PlayerStatisticService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FetchPlayersDataCommand extends Command
{
    private $playerStatisticService;
    private $playersToFetch;

    public function __construct(PlayerStatisticService $playerStatisticService, $playersToFetch)
    {
        parent::__construct();
        $this->playerStatisticService = $playerStatisticService;
        $this->playersToFetch = $playersToFetch;
    }

    protected function configure()
    {
        $this
            ->setName('steam:counter-strike:fetch-data')
            ->setDescription('Fetch new data from steam server');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        foreach ($this->playersToFetch as $player) {
            try {
                $this->playerStatisticService->fetchNewPlayerData(new FetchPlayerDataCommand($player));
                $output->writeln('Fetched player with id: ' . $player);

            } catch (Exception $e) {
                $output->writeln('Something went wrong with id: ' . $player);
            }
        }

        $output->writeln('Jobs done');
    }
}
