<?php

namespace Steam\CounterStrike\Infrastructure;

use Exception;
use GuzzleHttp\Exception\ClientException;
use Psr\Log\LoggerInterface;
use Steam\Command\UserStats\GetUserStatsForGame;
use Steam\CounterStrike\Domain\PlayerId;
use Steam\CounterStrike\Domain\PlayerStats;
use Steam\CounterStrike\Domain\PlayerStatsRepository;
use Steam\Steam;

class HttpPlayerStatsRepository implements PlayerStatsRepository
{
    private $steamApiKey;
    private $steam;
    private $logger;

    public function __construct($steamApiKey, Steam $steam, LoggerInterface $logger)
    {
        $this->steamApiKey = $steamApiKey;
        $this->steam = $steam;
        $this->logger = $logger;
    }

    public function findLastForPlayer(PlayerId $playerId)
    {
        try {
            $response = $this->steam->run(new GetUserStatsForGame($playerId->getId(), 730))->wait()->json();
        } catch (ClientException $e) {
            $this->logger->error($e->getMessage(), ['steam.counter_strike.http_stats_repository']);

            return null;
        }

        $playerStatsTransformer = new PlayerStatsTransformer();
        $playerStats = $playerStatsTransformer->toPlayerStatsFromApiResponse($response['playerstats']['stats'], $playerId);

        return $playerStats;
    }

    public function findAll()
    {
        throw new Exception('Find all not supported repository read only.');
    }

    public function save(PlayerStats $playerStats)
    {
        throw new Exception('Save not supported repository read only.');
    }
}
